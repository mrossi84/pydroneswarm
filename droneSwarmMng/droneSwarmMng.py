## Import ##

import time, sys, os, logging, datetime, select
import threading, multiprocessing, argparse 
import subprocess, dronekit, pymavlink, random

## ------ Section End ------ ##

## Constants ##

uav_n   = 2							# number of drones in the swarm
ctrl_l  = 0							# swarm control law identifier
fname   = "droneSwarmMng"			# name of the log file
log_l   = logging.DEBUG				# log level
ver     = 'v0.5'					# program version
base_ip = "127.0.0."				# base ip address for ad-hoc network
init_ip = 2                         # last octet for ip used by drones, to obtain an ipv4 address base_ip.init_ip
base_ctrl_port  = 14550				# base ip:port form mavlink
base_sens_port  = 5760				# base ip:port for sensing task

## ------ Section End ------ ##

## Drone object definition ##

class aDroneIF( multiprocessing.Process ):
	def __init__(self, id, ns, d_e_l, d1, d2, group = None, target = None, name = "aDrone", args = (), kwargs = {}):
		# save inputs to local variables
		self.name = name
		self.myid = id
		self.myip = base_ip+str(id+init_ip)
		self.my_control_port = str(base_ctrl_port+init_ip)
		self.my_sens_port = str(base_sens_port+init_ip)
		self.ns = ns
		self.de_l = d_e_l
		self.d1 = d1
		self.d2 = d2
		self.current_ps = 0
		self.next_ps = 0
		self.sens_dict = {'gas0': 0, 'gas1': 1, 'gas2': 2}
		logging.info('%s.%s init with ip: %s:%s|%s', self.name, self.myid, self.myip, self.my_control_port, self.my_sens_port)
		if log_l >= logging.DEBUG: print '[INFO] %s.%s init with ip: %s:%s|%s' % (self.name, self.myid, self.myip, self.my_control_port, self.my_sens_port)
		# open sockets towards corresponding UAV
		# control
		# self.vehicle = dronekit.connect(self.myip+":"+self.my_control_port, wait_ready=True)
		# sensing
		# launch a thread that manages sensing data
		super(aDroneIF, self).__init__(group, target, name, args, kwargs)

	def run(self):
		logging.info('%s.%s starting', self.name, self.myid)
		# wait until main loop initialization completes
		while not self.ns.running:
			time.sleep(0.25)
			continue
		# start 
		logging.info('%s.%s running', self.name, self.myid)
		while self.ns.running: # implement drone management
			#self.de_l() random.randint()
			# implement data collection from uav
			# GPS + flight parameters
			# sensors' data
			time.sleep(0.5)
			self.d1[self.myid] = random.randint(1, 10)
			self.sens_dict['gas0'] = random.randint(11, 20)
			self.sens_dict['gas1'] = random.randint(11, 20)
			self.sens_dict['gas2'] = random.randint(11, 20)
			self.d2[self.myid] = self.sens_dict
			self.de_l[self.myid] = 1
			logging.debug('%s.%s - d1:%s d2:%s de_l:%s', self.name, self.myid, self.d1[self.myid], self.d2[self.myid], self.de_l[self.myid])
			if log_l == logging.DEBUG: print "[DEBUG] %s.%s - d1:%s d2:%s de_l:%s" % (self.name, self.myid, self.d1[self.myid], self.d2[self.myid], self.de_l[self.myid])
		logging.info('%s.%s closing', self.name, self.myid)
		if log_l >= logging.DEBUG: print "[INFO] %s.%s closing" % (self.name, self.myid)
		return

## ------ Class End ------ ##

## Swarm Manager object definition ##

class swarmManager( multiprocessing.Process ):
	def __init__(self, ns, d_l, d_e_l, d1, d2, group = None, target = None, name = "swarmManager", args = (), kwargs = {}):
		self.ns = ns
		self.dl = d_l
		self.de_l = d_e_l
		self.d1 = d1
		self.d2 = d2
		self.name = name
		logging.info('[%s] initialized', self.name)
		super(swarmManager, self).__init__(group, target, name, args, kwargs)

	def run(self):
		logging.info('[%s] starting', self.name)
		# wait until main loop initialization completes
		while not self.ns.running:
			time.sleep(0.25)
			continue
		# running control loop
		logging.info('[%s] running', self.name)
		while self.ns.running: 
			#print "[DEBUG] %s working" % (self.name)
			## use the following to send messages to drones'ip directly from the manager
			#for d in self.dl:
			#	print "[INFO] Sending msg to %s" % (base_ip+str(d))
			## use the following to send messages to drone processes
			#for ev in self.de_l:
			#	if ev is not 0:
			#		self.d1 = random.randint(1, 10)
			for i in range(uav_n):
				if self.de_l[i]:
					logging.debug('[%s] drone:%s d1:%s d2:%s', self.name, i, self.d1[i], self.d2[i] )
					if log_l == logging.DEBUG: 
						print "[DEBUG][%s] drone:%s d1:%s d2:%s" % (self.name, i, self.d1[i],self.d2[i])
						print "[DEBUG][%s] drone:%s d2.gas0:%s" % (self.name, i, self.d2[i]['gas0'])
					self.de_l[i] = 0
			time.sleep(0.001)
		logging.info('[%s] closing', self.name)
		if log_l >= logging.DEBUG: print "[INFO][%s] closing" % (self.name)
		return

## ------ Class End ------ ##

## Function to convert system date-time into string ##

def dateToString():
	dt = datetime.datetime.today()
	month = str(dt.month) if dt.month > 9 else '0'+str(dt.month)
	day   = str(dt.day) if dt.day > 9 else '0'+str(dt.day)
	hour  = str(dt.hour) if dt.hour > 9 else '0'+str(dt.hour)
	min   = str(dt.minute) if dt.minute > 9 else '0'+str(dt.minute)
	sec   = str(dt.second) if dt.second > 9 else '0'+str(dt.second)
	res   = {'year':str(dt.year), 'month':month, 'day':day, 'hour':hour,
			'min':min, 'sec':sec }
	return res

## Function that parses input command ##

def runCommand(command):
	res = 0
	# First check
	if command == '' or command == '\n' or command == '\r':
		pass
	# Close the system correctly
	elif command == 'exit':
		res = 1
	# print the help
	else:
		print '-----------------\n\r',\
			'available command\n\r',\
			'exit - closes sockets and exit\n\r',\
			'-----------------\n\r'
		pass
	return res

## ------ Section End ------ ##

## Entry Point ##

if __name__ == "__main__":

	mgr   = multiprocessing.Manager()
	drone_id_list = mgr.list()			# shared list of drone ids
	drone_evnt_list = mgr.list()        # shared list of drone events
	drone_d1_list = mgr.list()			# shared list for data exchange
	drone_d2_list = mgr.list()          # shared list for data exchange
	d_l = []	 						# list of drone objects managed by main process
	g_var = mgr.Namespace()
	g_var.running = False				# keyboard

	parser = argparse.ArgumentParser()
	parser.add_argument('-n', '--number_d', type=int, action='store', dest='number_d', 
                         help='Set the number of drones in the swarm', default=3)
	parser.add_argument('-c', '--ctrl_law', type=int, action='store', dest='ctrl_law',
					 help='Select the swarm control law, 0:mapping (default), 1:fleet', default=0)
	parser.add_argument('-l', '--log_level', type=int, action='store', dest= 'loglevel', 
                         help='Define log level: 2-debug, 1-info, default warning', default=logging.INFO)
	parser.add_argument('--version', action='version', version='%(prog)s '+ver)
	param_list = parser.parse_args()

	dt = dateToString()
	f = './'+fname+'_'+ver+'_'+dt['year']+dt['month']+dt['day']+'-'+dt['hour']+dt['min']+dt['sec']+'.log'
	logging.basicConfig(filename=f, level=log_l, format='[%(asctime)s][%(levelname)s] %(message)s')

	if param_list.loglevel == 2:
		log_l = logging.DEBUG
	elif param_list.loglevel == 1:
		log_l = logging.INFO
	else:
		log_l = logging.WARNING
	logging.info('Set log level to: %s', log_l)

	uav_n  = param_list.number_d
	logging.info('Set size of swarm to: %s', uav_n)
	ctrl_l = param_list.ctrl_law
	logging.info('Set control law to: %s', ctrl_l)

	print '[INFO] Starting %s %s with params %d %d %d' % (fname,ver,log_l,uav_n,ctrl_l)

	try:
		# ping each drone to see if they've joined the network
		with open("null.txt","w") as null_f:
			for ping in range(uav_n):
				#address = base_ip + str(ping+init_ip)
				#res = subprocess.call(['ping', '-n', '1', address], stdout=null_f, stderr = null_f )
				#if res == 0:
				#	print "[INFO] ping to", address, "OK"
				#	logging.info("ping to %s : OK", address)
				#	d = aDrone(ping,args=(1,2,3))
				#	drone_id_list.append(d)
				#	d.start()
				#elif res == 2:
				#	print "[INFO] no response from", address
				#	logging.info("ping to %s : NO-RESP", address)
				#else:
				#	print "[INFO] ping to", address, "failed!"
				#	logging.info("ping to %s : FAIL", address)
				drone_evnt_list.append(0)
				drone_d1_list.append(0)
				drone_d2_list.append(0)
				d = aDroneIF(ping, g_var, drone_evnt_list, drone_d1_list, drone_d2_list)
				d_l.append(d)
				drone_id_list.append(ping)
				d.start()

		sm_p = swarmManager(g_var, drone_id_list, drone_evnt_list, drone_d1_list, drone_d2_list)
		sm_p.start()

		g_var.running = True

		while g_var.running:
			in_c = sys.stdin.readline().strip()
			if not (in_c == '' or in_c == ""):
				logging.info("input: %s", in_c)
				if runCommand(in_c) > 0:
					g_var.running = 0
			time.sleep(0.1)
	except KeyboardInterrupt:
		g_var.running = 0
		logging.warning('Keyboard interrupt! Safely Closing')
		if log_l >= logging.DEBUG: print("Keyboard Interrupt! Safely Closing")
	except Exception, e:
		print 'Exception', e

	for i in d_l:
		i.join()

	sm_p.join()

	logging.info("Done")
	print 'End'